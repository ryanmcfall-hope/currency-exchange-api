import requests
import json

baseUrl = 'http://apilayer.net/api/'

#  Read the key
with open("access_key.txt") as keyfile:
  key = keyfile.readline().strip()

#  Load the list of available currencies
listUrl = baseUrl + 'list'
response = requests.get(listUrl, params={'access_key': key})	
#  Convert the response into a Python dictionary 
#  The 'Types' chapter in the book describes dictionaries
currency_dictionary = json.loads(response.content)['currencies']

#  Print them all out (for now, this could be better later!)
for code in sorted(currency_dictionary.keys()):
	#  The encoding stuff here is because the currency names have
	#  Unicode characters that can't be printed
	print code, currency_dictionary[code].encode('ascii', 'ignore')
	
#  Ask the user for an amount in dollars to convert
amount = float(raw_input('How much money do you want to convert? '))	

#  Ask the user for the code for the country they want
country_code = raw_input('Enter code for the country you want: ')

#  Request the current exchange rate for that country
quoteUrl = baseUrl + 'live'

response = requests.get(quoteUrl, params={'access_key': key, 'currencies': country_code})

rate = json.loads(response.content)['quotes']['USD' + country_code]

print amount, 'American dollars is', amount*rate, currency_dictionary[country_code]


